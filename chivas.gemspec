# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'chivas/version'

Gem::Specification.new do |spec|
  spec.name          = "chivas"
  spec.version       = Chivas::VERSION
  spec.authors       = ["Bala Paranj"]
  spec.email         = ["bparanj@gmail.com"]

  spec.summary       = %q{This gem simplifies credit card purchase using Stripe API.}
  spec.description   = %q{This gem can be used to implement guest checkout and one click checkout using Stripe API.}
  spec.homepage      = "https://www.rubyplus.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'stripe', "~> 1.43"  

  spec.add_development_dependency "bundler", "~> 1.12.5"
  spec.add_development_dependency "rake", "~> 10.5"
  spec.add_development_dependency "minitest", "~> 5.8"
  spec.add_development_dependency 'filly', "~> 0.1.1"  
end
