# Chivas

This gem simplifies charging customers for a credit card purchase using Stripe API. To experiment with the code, run `bin/console` for an interactive prompt.

## Features

The client can implement guest checkout and one-click checkout using the charge method in this gem. To implement guest checkout, the client has to:
     1. Create token for credit card. This comes from the UI.
     2. Save stripe_customer_token in the database for one-click checkout. The interaction with Stripe server is implemented in filly gem that makes the call to save credit card token on Stripe server and return stripe_customer_token.
     3. Charge customer using the stripe_customer_token we get from step 2 for the first purchase

To implement one-click checkout, the client has to:
     1. Charge customer using the stripe_customer_token that was saved in the database when they purchased for the first time.
 
## Installation

Add this line to your application's Gemfile:

```ruby
gem 'chivas'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install chivas

## Usage

```ruby
charge = Chivas::CreditCard.charge(amount_in_cents, stripe_customer_token) 
```

Refer the sample Rails app that uses this gem : https://bitbucket.org/bparanj/striped

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/bparanj/chivas.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

