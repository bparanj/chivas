require_relative 'test_helper'

class ChargeTest < Minitest::Test
  
  def setup
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']
  end
  
  def test_charge_a_customer_for_a_given_amount
    token = Stripe::Token.create(:card => {
                                    :number    => "4242424242424242",
                                    :exp_month => 11,
                                    :exp_year  => 2025,
                                    :cvc       => "314"})  
    customer = Filly::CreditCard.save(token.id, 'test credit card')
    charge = Chivas::CreditCard.charge(1500, customer.id) 
    
    assert charge.id.size > 4
  end

end
