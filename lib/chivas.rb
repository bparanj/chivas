require 'stripe'
require 'chivas/version'
require 'filly/credit_card'
require 'chivas/credit_card'

module Chivas
  STRIPE_API_VERSION = '2015-09-03 (latest)'  
end
