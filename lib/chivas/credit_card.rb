module Chivas
  class CreditCard
    #
    # To implement guest checkout, the client has to:
    #
    #  1. Create token for credit card (comes from the UI)
    #  2. Save credit card token in the database for one-click checkout for subsequent purchases
    #  3. Charge customer using the stripe_customer_id for the current one-time purchase
    #
    # To implement one-click checkout, the client has to:
    #  1. Charge customer using the stripe_customer_id
    # 
    # The currency by default is USD
    def self.charge(amount, stripe_customer_id)
      Stripe::Charge.create(amount: amount, currency: "usd", customer: stripe_customer_id)
    end
  end
end